<?php
/**
 * @file
 * Create a parallaxe display fast and simple inside panels.
 */

/**
 * Implements hook_menu().
 */
function panel_parallaxe_menu() {
  $items['admin/config/media/para'] = array(
    'title' => 'Configure Parallaxe CSS',
    'description' => 'Define the CSS to use to order and arrange panel blocks.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('panel_parallaxe_admin_form'),
    'access arguments' => array('administer image styles'),
    'file' => 'panel_parallaxe.admin.inc',
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

/**
 * Implements hook_entity_info_alter().
 */
function panel_parallaxe_entity_info_alter(&$entity_info) {
  $entity_info['node']['view modes']['para'] = array(
    'label' => t('Parallaxe display'),
    'custom settings' => TRUE,
  );
}

/**
 * Implements hook_ctools_plugin_directory().
 */
function panel_parallaxe_ctools_plugin_directory($owner, $plugin_type) {
  if ($owner == 'panels' && $plugin_type == 'layouts') {
    return "plugins/$plugin_type";
  }
}

/**
 * Implements hook_node_view().
 */
function panel_parallaxe_node_view($node, $view_mode) {
  if ($view_mode == 'para') {
    if (variable_get('panel_parallaxe_css', 1) != 0) {
      drupal_add_css(drupal_get_path('module', 'panel_parallaxe') . '/css/panel_parallaxe.panel.css', array('every_page' => TRUE));
    }
    drupal_add_css(drupal_get_path('module', 'panel_parallaxe') . '/css/panel_parallaxe.para.css', array('every_page' => TRUE));
    drupal_add_js(drupal_get_path('module', 'panel_parallaxe') . '/js/panel_parallaxe.js');
    $node->content['#theme'] = 'parallaxe_view_theme';
  }
  return $node;
}

/**
 * Implements hook_theme().
 */
function panel_parallaxe_theme() {
  return array(
    'parallaxe_view_theme' => array(
      'variables' => array('node' => NULL),
    ),
    'parallaxe' => array(
      'variables' => array('parallaxe' => NULL, 'body' => NULL),
      'template' => 'panel_parallaxe',
    ),
  );
}

/**
 * Implements parallaxe_view theme function.
 */
function theme_parallaxe_view_theme($variables) {
  $node = $variables['node'];
  $styles = array(
    'parallaxe_l_640_480',
    'parallaxe_l_720_540',
    'parallaxe_l_800_600',
    'parallaxe_l_1024_768',
    'parallaxe_l_1280_960',
    'parallaxe_l_1366_1024',
    'parallaxe_l_1920_1080',
    'parallaxe_p_480_640',
    'parallaxe_p_540_720',
    'parallaxe_p_600_800',
    'parallaxe_p_768_1024',
    'parallaxe_p_960_1280',
    'parallaxe_p_1024_1366',
    'parallaxe_p_1080_1920',
  );
  foreach ($styles as $style) {
    $parallaxe[$node->parallaxe[$node->language][0]['filename']][$style] = image_style_url($style, $node->parallaxe[$node->language][0]['uri']);
  }
  drupal_add_js(array('panel_parallaxe' => $parallaxe), 'setting');
  $output = theme('parallaxe', array(
      'parallaxe' => $parallaxe[$node->parallaxe[$node->language][0]['filename']]['parallaxe_l_1920_1080'],
      'body' => check_markup($node->body[$node->language][0]['value'], 2, '', FALSE),
    )
  );
  return $output;
}

/**
 * Implements hook_views_api().
 */
function panel_parallaxe_views_api($module = NULL, $api = NULL) {
  return array('api' => '3.0');
}

/**
 * Implements hook_image_default_styles().
 */
function panel_parallaxe_image_default_styles() {
  $styles = array();

  // Exported image style: parallaxe_l_1024_768.
  $styles['parallaxe_l_1024_768'] = array(
    'name' => 'parallaxe_l_1024_768',
    'label' => 'parallaxe_l_1024_768',
    'effects' => array(
      10 => array(
        'label' => 'Scale and Smart Crop',
        'help' => 'Similar to "Scale And Crop", but preserves the portion of the image with the most entropy.',
        'effect callback' => 'smartcrop_scale_effect',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'smartcrop',
        'name' => 'smartcrop_scale_and_crop',
        'data' => array(
          'width' => 1024,
          'height' => 768,
          'upscale' => 1,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: parallaxe_l_1280_960.
  $styles['parallaxe_l_1280_960'] = array(
    'name' => 'parallaxe_l_1280_960',
    'label' => 'parallaxe_l_1280_960',
    'effects' => array(
      9 => array(
        'label' => 'Scale and Smart Crop',
        'help' => 'Similar to "Scale And Crop", but preserves the portion of the image with the most entropy.',
        'effect callback' => 'smartcrop_scale_effect',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'smartcrop',
        'name' => 'smartcrop_scale_and_crop',
        'data' => array(
          'width' => 1280,
          'height' => 960,
          'upscale' => 1,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: parallaxe_l_1366_1024.
  $styles['parallaxe_l_1366_1024'] = array(
    'name' => 'parallaxe_l_1366_1024',
    'label' => 'parallaxe_l_1366_1024',
    'effects' => array(
      8 => array(
        'label' => 'Scale and Smart Crop',
        'help' => 'Similar to "Scale And Crop", but preserves the portion of the image with the most entropy.',
        'effect callback' => 'smartcrop_scale_effect',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'smartcrop',
        'name' => 'smartcrop_scale_and_crop',
        'data' => array(
          'width' => 1366,
          'height' => 1024,
          'upscale' => 1,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: parallaxe_l_1920_1080.
  $styles['parallaxe_l_1920_1080'] = array(
    'name' => 'parallaxe_l_1920_1080',
    'label' => 'parallaxe_l_1920_1080',
    'effects' => array(
      7 => array(
        'label' => 'Scale and Smart Crop',
        'help' => 'Similar to "Scale And Crop", but preserves the portion of the image with the most entropy.',
        'effect callback' => 'smartcrop_scale_effect',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'smartcrop',
        'name' => 'smartcrop_scale_and_crop',
        'data' => array(
          'width' => 1920,
          'height' => 1080,
          'upscale' => 1,
        ),
        'weight' => 3,
      ),
    ),
  );

  // Exported image style: parallaxe_l_640_480.
  $styles['parallaxe_l_640_480'] = array(
    'name' => 'parallaxe_l_640_480',
    'label' => 'parallaxe_l_640_480',
    'effects' => array(
      13 => array(
        'label' => 'Scale and Smart Crop',
        'help' => 'Similar to "Scale And Crop", but preserves the portion of the image with the most entropy.',
        'effect callback' => 'smartcrop_scale_effect',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'smartcrop',
        'name' => 'smartcrop_scale_and_crop',
        'data' => array(
          'width' => 640,
          'height' => 480,
          'upscale' => 1,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: parallaxe_l_720_540.
  $styles['parallaxe_l_720_540'] = array(
    'name' => 'parallaxe_l_720_540',
    'label' => 'parallaxe_l_720_540',
    'effects' => array(
      12 => array(
        'label' => 'Scale and Smart Crop',
        'help' => 'Similar to "Scale And Crop", but preserves the portion of the image with the most entropy.',
        'effect callback' => 'smartcrop_scale_effect',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'smartcrop',
        'name' => 'smartcrop_scale_and_crop',
        'data' => array(
          'width' => 720,
          'height' => 540,
          'upscale' => 1,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: parallaxe_l_800_600.
  $styles['parallaxe_l_800_600'] = array(
    'name' => 'parallaxe_l_800_600',
    'label' => 'parallaxe_l_800_600',
    'effects' => array(
      11 => array(
        'label' => 'Scale and Smart Crop',
        'help' => 'Similar to "Scale And Crop", but preserves the portion of the image with the most entropy.',
        'effect callback' => 'smartcrop_scale_effect',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'smartcrop',
        'name' => 'smartcrop_scale_and_crop',
        'data' => array(
          'width' => 800,
          'height' => 600,
          'upscale' => 1,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: parallaxe_p_1024_1366.
  $styles['parallaxe_p_1024_1366'] = array(
    'name' => 'parallaxe_p_1024_1366',
    'label' => 'parallaxe_p_1024_1366',
    'effects' => array(
      15 => array(
        'label' => 'Scale and Smart Crop',
        'help' => 'Similar to "Scale And Crop", but preserves the portion of the image with the most entropy.',
        'effect callback' => 'smartcrop_scale_effect',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'smartcrop',
        'name' => 'smartcrop_scale_and_crop',
        'data' => array(
          'width' => 1024,
          'height' => 1366,
          'upscale' => 1,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: parallaxe_p_1080_1920.
  $styles['parallaxe_p_1080_1920'] = array(
    'name' => 'parallaxe_p_1080_1920',
    'label' => 'parallaxe_p_1080_1920',
    'effects' => array(
      14 => array(
        'label' => 'Scale and Smart Crop',
        'help' => 'Similar to "Scale And Crop", but preserves the portion of the image with the most entropy.',
        'effect callback' => 'smartcrop_scale_effect',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'smartcrop',
        'name' => 'smartcrop_scale_and_crop',
        'data' => array(
          'width' => 1080,
          'height' => 1920,
          'upscale' => 1,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: parallaxe_p_480_640.
  $styles['parallaxe_p_480_640'] = array(
    'name' => 'parallaxe_p_480_640',
    'label' => 'parallaxe_p_480_640',
    'effects' => array(
      20 => array(
        'label' => 'Scale and Smart Crop',
        'help' => 'Similar to "Scale And Crop", but preserves the portion of the image with the most entropy.',
        'effect callback' => 'smartcrop_scale_effect',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'smartcrop',
        'name' => 'smartcrop_scale_and_crop',
        'data' => array(
          'width' => 480,
          'height' => 640,
          'upscale' => 1,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: parallaxe_p_540_720.
  $styles['parallaxe_p_540_720'] = array(
    'name' => 'parallaxe_p_540_720',
    'label' => 'parallaxe_p_540_720',
    'effects' => array(
      19 => array(
        'label' => 'Scale and Smart Crop',
        'help' => 'Similar to "Scale And Crop", but preserves the portion of the image with the most entropy.',
        'effect callback' => 'smartcrop_scale_effect',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'smartcrop',
        'name' => 'smartcrop_scale_and_crop',
        'data' => array(
          'width' => 540,
          'height' => 720,
          'upscale' => 1,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: parallaxe_p_600_800.
  $styles['parallaxe_p_600_800'] = array(
    'name' => 'parallaxe_p_600_800',
    'label' => 'parallaxe_p_600_800',
    'effects' => array(
      18 => array(
        'label' => 'Scale and Smart Crop',
        'help' => 'Similar to "Scale And Crop", but preserves the portion of the image with the most entropy.',
        'effect callback' => 'smartcrop_scale_effect',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'smartcrop',
        'name' => 'smartcrop_scale_and_crop',
        'data' => array(
          'width' => 600,
          'height' => 800,
          'upscale' => 1,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: parallaxe_p_768_1024.
  $styles['parallaxe_p_768_1024'] = array(
    'name' => 'parallaxe_p_768_1024',
    'label' => 'parallaxe_p_768_1024',
    'effects' => array(
      17 => array(
        'label' => 'Scale and Smart Crop',
        'help' => 'Similar to "Scale And Crop", but preserves the portion of the image with the most entropy.',
        'effect callback' => 'smartcrop_scale_effect',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'smartcrop',
        'name' => 'smartcrop_scale_and_crop',
        'data' => array(
          'width' => 768,
          'height' => 1024,
          'upscale' => 1,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: parallaxe_p_960_1280.
  $styles['parallaxe_p_960_1280'] = array(
    'name' => 'parallaxe_p_960_1280',
    'label' => 'parallaxe_p_960_1280',
    'effects' => array(
      16 => array(
        'label' => 'Scale and Smart Crop',
        'help' => 'Similar to "Scale And Crop", but preserves the portion of the image with the most entropy.',
        'effect callback' => 'smartcrop_scale_effect',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'smartcrop',
        'name' => 'smartcrop_scale_and_crop',
        'data' => array(
          'width' => 960,
          'height' => 1280,
          'upscale' => 1,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}
