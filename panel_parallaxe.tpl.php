<?php
/**
 * @file
 * The theme template for the parallaxe display.
 */
?>

<section class="module parallax" style="background-image: url('<?php echo $parallaxe; ?>');">
  <div class="opacity">
    <div class="container">
      <?php echo $body; ?>
    </div>
  </div>
</section>
